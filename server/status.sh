#!/bin/bash

echo "Connecting to the DiVTB Server at $DIVTB_SERVER_SSH_HOST:$DIVTB_SERVER_SSH_PORT as \`$DIVTB_SERVER_SSH_USER\`"
DIVTB_RESULT=$(sshpass -p $DIVTB_SERVER_SSH_PASSWORD ssh $DIVTB_SERVER_SSH_USER@$DIVTB_SERVER_SSH_HOST -p$DIVTB_SERVER_SSH_PORT "
cd $DIVTB_SERVER_ROOT_DIR

PID=\"LAST_PID\"
if [ ! -e \$PID ]; then
	echo -e \"\e[0;31mDiVTB Server not running (no PID file)\e[0m\"
	exit
fi

PIDV=\$(cat \$PID)
if ps axww | grep -v grep | grep \$PIDV > /dev/null 2>&1 ; then
	echo -e \"\e[0;32mDiVTB Server running with PID \${PIDV}\e[0m\"
	exit
fi

echo -e \"\e[0;33mWarning: DiVTB Server not running, but PID file \$PID found\e[0m\"")
