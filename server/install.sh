#!/bin/bash

echo "Connecting to the DiVTB Server at $DIVTB_SERVER_SSH_HOST:$DIVTB_SERVER_SSH_PORT as \`$DIVTB_SERVER_SSH_USER\`"

sshpass -p $DIVTB_SERVER_SSH_PASSWORD ssh -t $DIVTB_SERVER_SSH_USER@$DIVTB_SERVER_SSH_HOST -p$DIVTB_SERVER_SSH_PORT "
set -e

echo \"Install DiVTB Server dependencies:\"
echo $DIVTB_SERVER_SSH_PASSWORD | sudo -S zypper in java-1_7_0-openjdk-devel git
wget ftp://ftp.pbone.net/mirror/ftp5.gwdg.de/pub/opensuse/repositories/home:/a_richardson/openSUSE_Tumbleweed/noarch/maven-3.1.0-2.1.noarch.rpm -O maven-3.1.0-2.1.noarch.rpm
sudo rpm -ifvh --replacepkgs maven-3.1.0-2.1.noarch.rpm
rm maven-3.1.0-2.1.noarch.rpm

mkdir -p $DIVTB_SERVER_ROOT_DIR
cd $DIVTB_SERVER_ROOT_DIR
echo \"Cloning DiVTB Server from $DIVTB_SERVER_REPOSITORY\"
git clone $DIVTB_SERVER_REPOSITORY .
mvn clean install assembly:single

mkdir $DIVTB_SERVER_ROOT_DIR/projects
mkdir $DIVTB_SERVER_ROOT_DIR/work
mkdir $DIVTB_SERVER_ROOT_DIR/results
mkdir $DIVTB_SERVER_ROOT_DIR/results_zip

mkdir /home/$DIVTB_SERVER_SSH_USER/.caebeans
mkdir /home/$DIVTB_SERVER_SSH_USER/.caebeans/certs

echo \"admin.port = 9001

caeserver.host = http://$DIVTB_SERVER_SSH_HOST
caeserver.port = 9000
caeserver.pool_size = 10

#	Properties for storage

storage.projects = $DIVTB_SERVER_ROOT_DIR/projects
storage.work = $DIVTB_SERVER_ROOT_DIR/work
storage.results = $DIVTB_SERVER_ROOT_DIR/results
storage.results_zip_dir = $DIVTB_SERVER_ROOT_DIR/results_zip

#	Client properties

wsrf.ssl.keystore = /home/$DIVTB_SERVER_SSH_USER/.caebeans/certs/demouser.jks
wsrf.ssl.keypass = the!user
wsrf.ssl.truststore = /home/$DIVTB_SERVER_SSH_USER/.caebeans/certs/demouser.jks
wsrf.ssl.trustpass = the!user

unicore.registry_url = $DIVTB_SERVER_REGISTRY_URL
broker.url = $DIVTB_SERVER_BROKER_URL\" > /home/$DIVTB_SERVER_SSH_USER/.caebeans/caeserver.properties

echo \"log4j.rootLogger= CA, FA

#Console Appender
log4j.appender.CA=org.apache.log4j.ConsoleAppender
log4j.appender.CA.layout=org.apache.log4j.PatternLayout
log4j.appender.CA.layout.ConversionPattern=%-4r [%t] %-5p %c %x - %m%n

log4j.appender.СA.Threshold = WARN
 
#File Appender
log4j.appender.FA=org.apache.log4j.FileAppender
log4j.appender.FA.File=logs/caeserver.log
log4j.appender.FA.layout=org.apache.log4j.PatternLayout
log4j.appender.FA.layout.ConversionPattern=%-4r [%t] %-5p %c %x - %m%n
  
# Set the logger level of File Appender to WARN
log4j.appender.FA.Threshold = WARN\" > /home/$DIVTB_SERVER_SSH_USER/.caebeans/logger.properties

exit"

if [ $? -ne 0 ]; then
	DIVTB_RESULT="\e[0;31mDiVTB Server not installed\e[0m"
else
	sshpass -p $DIVTB_SERVER_SSH_PASSWORD scp -P$DIVTB_SERVER_SSH_PORT server/demouser.jks $DIVTB_SERVER_SSH_USER@$DIVTB_SERVER_SSH_HOST:/home/$DIVTB_SERVER_SSH_USER/.caebeans/certs/
	if [ $? -ne 0 ]; then
		DIVTB_RESULT="\e[0;31mDiVTB Server not installed\e[0m"
	else
		DIVTB_RESULT="\e[0;32mDiVTB Server installed\e[0m"
	fi
fi
