#!/bin/bash

echo "Connecting to the DiVTB Server at $DIVTB_SERVER_SSH_HOST:$DIVTB_SERVER_SSH_PORT as \`$DIVTB_SERVER_SSH_USER\`"
DIVTB_RESULT=$(sshpass -p $DIVTB_SERVER_SSH_PASSWORD ssh $DIVTB_SERVER_SSH_USER@$DIVTB_SERVER_SSH_HOST -p$DIVTB_SERVER_SSH_PORT "
cd $DIVTB_SERVER_ROOT_DIR

PID=\"LAST_PID\"
if [ ! -e \$PID ]; then
	echo -e \"\e[0;31mNo PID file found, server probably already stopped.\e[0m\"
	exit
fi

PIDV=\$(cat \$PID)
cat \$PID | xargs kill -SIGTERM

STOPPED="no"
until [ \"\$STOPPED\" = \"\" ]; do
	STOPPED=\$(ps -p \$PIDV | grep \$PIDV)
    if [ \$? != 0 ]; then
		STOPPED=\"\"
	fi
	sleep 2
done
rm -f \$PID
echo -e \"\e[0;32mDiVTB Server stopped\e[0m\"");
