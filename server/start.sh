#!/bin/bash

echo "Connecting to the DiVTB Server at $DIVTB_SERVER_SSH_HOST:$DIVTB_SERVER_SSH_PORT as \`$DIVTB_SERVER_SSH_USER\`"
DIVTB_RESULT=$(sshpass -p $DIVTB_SERVER_SSH_PASSWORD ssh $DIVTB_SERVER_SSH_USER@$DIVTB_SERVER_SSH_HOST -p$DIVTB_SERVER_SSH_PORT "
cd $DIVTB_SERVER_ROOT_DIR

PID=\"LAST_PID\"
if [ -e \$PID ]; then
	if [ -d /proc/\$(cat \$PID) ]; then
		echo -e \"\e[0;33mDiVTB Server instance may be already running with process id \"\$(cat \$PID)\"\e[0m\"
		echo \"If this is not the case, delete the file \$PID and re-run this script\"
		exit
	fi
fi

CLASSPATH=\$CP; export CLASSPATH
nohup java -jar target/TaskBuilder-0.2-jar-with-dependencies.jar > logs/work.log 2>&1  & echo \$! > \${PID}

echo -e \"\e[0;32mDiVTB Server started\e[0m\"");
