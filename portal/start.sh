#!/bin/bash

echo "Connecting to the DiVTB Portal at $DIVTB_PORTAL_SSH_HOST:$DIVTB_PORTAL_SSH_PORT as \`$DIVTB_PORTAL_SSH_USER\`"
DIVTB_RESULT=$(sshpass -p $DIVTB_PORTAL_SSH_PASSWORD ssh $DIVTB_PORTAL_SSH_USER@$DIVTB_PORTAL_SSH_HOST -p$DIVTB_PORTAL_SSH_PORT "

if ps axww | grep -v grep | grep apache2 > /dev/null 2>&1 ; then
	echo -e \"\e[0;33mDiVTB Portal instance may be already running (apache2 already started)\e[0m\"
else
	echo $DIVTB_PORTAL_SSH_PASSWORD | sudo -S /etc/init.d/apache2 start
	echo -e \"\e[0;32mDiVTB Portal started\e[0m\"
fi");
