#!/bin/bash

echo "Connecting to the DiVTB Portal at $DIVTB_PORTAL_SSH_HOST:$DIVTB_PORTAL_SSH_PORT as \`$DIVTB_PORTAL_SSH_USER\`"

sshpass -p $DIVTB_PORTAL_SSH_PASSWORD ssh -t $DIVTB_PORTAL_SSH_USER@$DIVTB_PORTAL_SSH_HOST -p$DIVTB_PORTAL_SSH_PORT "
set -e

echo \"Install DiVTB Portal dependencies:\"
echo \" - PHP5 & Apache2 installation\"
echo $DIVTB_PORTAL_SSH_PASSWORD | sudo -S /sbin/yast2 -i apache2 apache2-mod_php5 php5-mysql php5-curl php5-dom php5-soap php5-sockets php5-sqlite php5-openssl php5-phar
sudo /sbin/chkconfig --add apache2
sudo /etc/init.d/apache2 start

sudo /usr/sbin/a2enmod rewrite
sudo /sbin/service apache2 restart

echo \" - VCS git installation\"
sudo zypper in git

echo \"Install DiVTB Portal:\"
mkdir -p $DIVTB_PORTAL_ROOT_DIR
cd $DIVTB_PORTAL_ROOT_DIR
echo \" - Clone DiVTB Portal from $DIVTB_PORTAL_REPOSITORY\"
git clone $DIVTB_PORTAL_REPOSITORY .

sudo su -c \"cat default-server.conf > /etc/apache2/default-server.conf\"
sudo /sbin/service apache2 restart

echo \" - Install Doctrine ORM\"
./doctrine-setup.sh

echo \" - Create DiVTB Portal database\"
cd $DIVTB_PORTAL_ROOT_DIR/www/protected
./setup.sh

echo \" - Configure DiVTB Portal\"
echo \"<?php
return array(
		'address' => '$DIVTB_SERVER_SSH_HOST',
		'port' => 9000,
		'adminPort' => 9001,
	);
?>\" > config/caeserver.php

echo \" - Publish DiVTB Portal to the web\"
sudo rm -rf /srv/www/htdocs
sudo cp -r $DIVTB_PORTAL_ROOT_DIR/www /srv/www/htdocs
sudo cp -r $DIVTB_PORTAL_ROOT_DIR/composer /srv/www/composer
sudo chown -R wwwrun:www /srv/www/htdocs
sudo chown -R wwwrun:www /srv/www/composer

exit"

if [ $? -ne 0 ]; then
	DIVTB_RESULT="DiVTB Portal not installed"
else
	DIVTB_RESULT="DiVTB Portal installed"
fi
