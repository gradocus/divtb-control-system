#!/bin/bash

if [ -f divtb.cfg ]; then
	source divtb.cfg
else # Load default config
	echo "Load default config"
fi

USAGE="usage: divtb.sh [module] command"

case $# in
	1)
		RESULTS=""
		for script in $(find */$1.sh); do
			source $script
			if [[ "$RESULTS" == "" ]]; then
				RESULTS=$DIVTB_RESULT
			else
				RESULTS="$RESULTS\n$DIVTB_RESULT"
			fi
		done
		if [[ "$RESULTS" != "" ]]; then
			echo -e $RESULTS
		fi
	;;
	2)
		if [ -f $1/$2.sh ]; then
			source $1/$2.sh
			echo -e $DIVTB_RESULT
		else
			echo $USAGE
			exit 1
		fi
	;;
	*)
		echo $USAGE
		exit 1
	;;
esac
