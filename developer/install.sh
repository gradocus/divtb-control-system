#!/bin/bash

echo "Connecting to the DiVTB Developer at $DIVTB_DEVELOPER_SSH_HOST:$DIVTB_DEVELOPER_SSH_PORT as \`$DIVTB_DEVELOPER_SSH_USER\`"

sshpass -p $DIVTB_DEVELOPER_SSH_PASSWORD ssh -t $DIVTB_DEVELOPER_SSH_USER@$DIVTB_DEVELOPER_SSH_HOST -p$DIVTB_DEVELOPER_SSH_PORT "
set -e

echo \"Install DiVTB Developer dependencies:\"
echo $DIVTB_DEVELOPER_SSH_PASSWORD | sudo -S zypper install python-xml python-pip
sudo pip install --upgrade pip
sudo pip install django==\"1.5.1\" south beautifulsoup4 requests
sudo zypper install git

mkdir -p $DIVTB_DEVELOPER_ROOT_DIR
cd $DIVTB_DEVELOPER_ROOT_DIR
echo \"Cloning DiVTB Developer from $DIVTB_DEVELOPER_REPOSITORY\"
git clone $DIVTB_DEVELOPER_REPOSITORY .

mkdir $DIVTB_DEVELOPER_ROOT_DIR/logs

echo \"DIVTB_URL=\"$DIVTB_DEVELOPER_HOST\"
PORT=\"$DIVTB_DEVELOPER_PORT\"
UPLOAD_PROJECT_WS_URL=\"http://$DIVTB_SERVER_SSH_HOST:9000/caeserver?wsdl\"\" > divtb_developer.conf

exit"

if [ $? -ne 0 ]; then
	DIVTB_RESULT="\e[0;31mDiVTB Developer not installed\e[0m"
else
	DIVTB_RESULT="\e[0;32mDiVTB Developer installed\e[0m"
fi
