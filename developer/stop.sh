#!/bin/bash

echo "Connecting to the DiVTB Developer at $DIVTB_DEVELOPER_SSH_HOST:$DIVTB_DEVELOPER_SSH_PORT as \`$DIVTB_DEVELOPER_SSH_USER\`"
DIVTB_RESULT=$(sshpass -p $DIVTB_DEVELOPER_SSH_PASSWORD ssh $DIVTB_DEVELOPER_SSH_USER@$DIVTB_DEVELOPER_SSH_HOST -p$DIVTB_DEVELOPER_SSH_PORT "
cd $DIVTB_DEVELOPER_ROOT_DIR
source $DIVTB_DEVELOPER_ROOT_DIR/divtb_developer.conf

PID=\"LAST_PID\"
if [ ! -e \$PID ]; then
	echo -e \"\e[0;31mNo PID file found, developer probably already stopped.\e[0m\"
	exit
fi

PIDV=\$(cat \$PID)
ps axww | grep -v grep | grep \"/usr/bin/python ./manage.py runserver \$DIVTB_URL:\$PORT\" | awk '{print \$1}' | xargs kill

STOPPED="no"
until [ \"\$STOPPED\" = \"\" ]; do
	STOPPED=\$(ps -p \$PIDV | grep \$PIDV)
    if [ \$? != 0 ]; then
		STOPPED=\"\"
	fi
	sleep 2
done
rm -f \$PID
echo -e \"\e[0;32mDiVTB Developer stopped\e[0m\"");
