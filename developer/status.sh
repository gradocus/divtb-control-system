#!/bin/bash

echo "Connecting to the DiVTB Developer at $DIVTB_DEVELOPER_SSH_HOST:$DIVTB_DEVELOPER_SSH_PORT as \`$DIVTB_DEVELOPER_SSH_USER\`"
DIVTB_RESULT=$(sshpass -p $DIVTB_DEVELOPER_SSH_PASSWORD ssh $DIVTB_DEVELOPER_SSH_USER@$DIVTB_DEVELOPER_SSH_HOST -p$DIVTB_DEVELOPER_SSH_PORT "
cd $DIVTB_DEVELOPER_ROOT_DIR

PID=\"LAST_PID\"
if [ ! -e \$PID ]; then
	echo -e \"\e[0;31mDiVTB Developer not running (no PID file found)\e[0m\"
	exit
fi

PIDV=\$(cat \$PID)
if ps axww | grep -v grep | grep \$PIDV > /dev/null 2>&1 ; then
	echo -e \"\e[0;32mDiVTB Developer running with PID \${PIDV}\e[0m\"
	exit
fi

echo -e \"\e[0;33mWarning: DiVTB Developer not running, but PID file \$PID found\e[0m\"");
