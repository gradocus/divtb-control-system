#!/bin/bash

echo "Connecting to the DiVTB Developer at $DIVTB_DEVELOPER_SSH_HOST:$DIVTB_DEVELOPER_SSH_PORT as \`$DIVTB_DEVELOPER_SSH_USER\`"
DIVTB_RESULT=$(sshpass -p $DIVTB_DEVELOPER_SSH_PASSWORD ssh $DIVTB_DEVELOPER_SSH_USER@$DIVTB_DEVELOPER_SSH_HOST -p$DIVTB_DEVELOPER_SSH_PORT "
cd $DIVTB_DEVELOPER_ROOT_DIR
source $DIVTB_DEVELOPER_ROOT_DIR/divtb_developer.conf

PID=\"LAST_PID\"
if [ -e \$PID ]; then
	if [ -d /proc/\$(cat \$PID) ]; then
		echo -e \"\e[0;33mDiVTB Developer instance may be already running with process id \"\$(cat \$PID)\"\e[0m\"
		echo \"If this is not the case, delete the file \$PID and re-run this script\"
		exit
	fi
fi

/usr/bin/python ./manage.py runserver \$DIVTB_URL:\$PORT > logs/django.log 2>&1 & echo \$! > \${PID}

echo -e \"\e[0;32mDiVTB Developer started\e[0m\"");
