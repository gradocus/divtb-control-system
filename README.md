# DiVTB Control System #

DiVTB Control System is a set of deployment scripts for a components of the [DiVTB](https://bitbucket.org/domage/caebeans) system:

 * [DiVTB Broker](https://bitbucket.org/shamakina/divtb_broker) (Private access, not supported yet)

 * [DiVTB Collector](https://bitbucket.org/SillyLizzy/divtb_collector) (Private access, not supported yet)

 * [DiVTB Developer](https://bitbucket.org/domage/divtb-developer) (Private access)

 * [DiVTB Portal](https://bitbucket.org/gradocus/caebeans-portal)

 * [DiVTB Server](https://bitbucket.org/skayred/caeserver-core)

## How to use ##

1. Clone repository:

		git clone https://bitbucket.org/gradocus/divtb-control-system.git

2. Set up your own settings in `divtb.cfg` file.

3. Use `divtb.sh` script as `./divtb.sh [COMPONENT] COMMAND`:

	* `COMPONENT` is name of DiVTB subsystem (e.g. `developer`, `portal` or `server`)

	* `COMMAND` is command defined for selected subsystem (e.g. `install`, `start`, `stop`, etc.)

	* You can use it for all DiVTB components which support required command: 

			./divtb.sh install # Run install command for each DiVTB subsystem

## License ##

Copyright (c) 2014, Eugene Zakharov

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. 
